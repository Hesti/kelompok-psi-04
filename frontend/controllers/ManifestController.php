<?php

namespace frontend\controllers;

use Yii;
use frontend\models\manifest;
use frontend\models\penumpang;
use frontend\models\search\manifestSearch;
use frontend\models\search\penumpangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManifestController implements the CRUD actions for manifest model.
 */
class ManifestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all manifest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new penumpangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single manifest model.
     * @param string $id_manifest
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($NIK, $barcode)
    {
        return $this->render('view', [
            'model' => $this->findModel($NIK, $barcode),
        ]);
    }

    /**
     * Creates a new manifest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($NIK, $barcode)
    {
        $model = new manifest();
        $penumpang = $this->findModel($NIK, $barcode);
        if ($model->load(Yii::$app->request->post())) {
            $model->barcode = $penumpang->barcode;
           //var_dump($model);die;
            return $this->redirect(['view', 'NIK'=>$penumpang->NIK,  'barcode' => $model->barcode]);
        }

        return $this->render('create', [
            'model' => $model,
            'penumpang' => $penumpang
        ]);
    }

    /**
     * Updates an existing manifest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $barcode)
    {
        $model = $this->findModel($id, $barcode);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'barcode' => $model->barcode]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing manifest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $barcode)
    {
        $this->findModel($id, $barcode)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the manifest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $barcode
     * @return manifest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($NIK, $barcode)
    {
        if (($model = penumpang::findOne(['NIK' => $NIK, 'barcode' => $barcode])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelPenumpang($id_penumpang)
    {
        if (($model = penumpang::findOne(['id' => $id, 'barcode' => $barcode])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
