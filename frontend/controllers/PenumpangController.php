<?php

namespace frontend\controllers;

use Yii;
use frontend\models\penumpang;
use frontend\models\search\penumpangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PenumpangController implements the CRUD actions for penumpang model.
 */
class PenumpangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all penumpang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new penumpangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single penumpang model.
     * @param string $barcode
     * @param integer $NIK
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($barcode, $NIK)
    {
        return $this->render('view', [
            'model' => $this->findModel($barcode, $NIK),
        ]);
    }

    /**
     * Creates a new penumpang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new penumpang();

        if ($model->load(Yii::$app->request->post())) 
        {
            $model->setId();
            $model->setBarcode();
            $model->id_user = Yii::$app->user->getId();
            return $this->redirect(['view', 'barcode' => $model->barcode, 'NIK' => $model->NIK]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing penumpang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $barcode
     * @param integer $NIK
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($barcode, $NIK)
    {
        $model = $this->findModel($barcode, $NIK);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'barcode' => $model->barcode, 'NIK' => $model->NIK]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing penumpang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $barcode
     * @param integer $NIK
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($barcode, $NIK)
    {
        $this->findModel($barcode, $NIK)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the penumpang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $barcode
     * @param integer $NIK
     * @return penumpang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($barcode, $NIK)
    {
        if (($model = penumpang::findOne(['barcode' => $barcode, 'NIK' => $NIK])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
