<?php

namespace frontend\controllers;

use Yii;
use frontend\models\harga_tiket;
use frontend\models\PerubahanHargaTiket;
use frontend\models\search\harga_tiketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Harga_tiketController implements the CRUD actions for harga_tiket model.
 */
class Harga_tiketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all harga_tiket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new harga_tiketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single harga_tiket model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new harga_tiket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new harga_tiket();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->setId();
            $model->save();
            return $this->redirect(['harga_tiket/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing harga_tiket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_perubahan = new PerubahanHargaTiket();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }
        $model_perubahan->setId();
        $model_perubahan->id_user = Yii::$app->user->getId();
        $model_perubahan->id_harga_tiket = $model->id;
        $model_perubahan->waktu_perubahan_harga = date('Y:m:d H:i:s',strtotime('+5Hours'));
        $model_perubahan->save();

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing harga_tiket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the harga_tiket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return harga_tiket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = harga_tiket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
