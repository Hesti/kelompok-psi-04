<?php

namespace frontend\controllers;

use Yii;
use frontend\models\tiket;
use frontend\models\search\tiketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\TiketForm;
use frontend\models\User;
use frontend\models\Jadwal;
use frontend\models\Penumpang;
use frontend\models\harga_tiket;
use frontend\models\kapal;
use kartik\mpdf\Pdf;

/**
 * TiketController implements the CRUD actions for tiket model.
 */
class TiketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all tiket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new tiketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single tiket model.
     * @param string $id_tiket
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_tiket, $barcode)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_tiket, $barcode),
        ]);
    }

    /**
     * Creates a new tiket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TiketForm();
        $user = new user();
        $tiket = new tiket();

        $hargatiket = new harga_tiket();
        $penumpang = new Penumpang();
        if ($model->load(Yii::$app->request->post())) {
             $penumpang->setBarcode();
             $penumpang->NIK = $model->NIK;
             $penumpang->id_user = Yii::$app->user->getId();
             $penumpang->jumlah_penumpang = $model->jumlah_penumpang;
             $penumpang->no_polisi = $model->no_polisi;

             $penumpang->save(); // solve :)
              $tiket->setId();
              $tiket->barcode =  $penumpang->barcode;
              $tiket->id_user = Yii::$app->user->getId();
            $kendaraan = harga_tiket::find()
                ->where(['id_harga_tiket' => $model->jenis_kendaraan])
                ->one();     
            $jadwalid = jadwal::find()
                ->where(['asal' => $model->asal, 'tujuan' => $model->tujuan, 'trip' =>$model->trip])
                ->one();
            $tiket->id_jadwal = $jadwalid->id_jadwal;
            $tiket->id_kapal = $model->kapal;
            $tiket->id_harga_tiket = $model->jenis_kendaraan;
            $jenis_kendaraan = $kendaraan->jenis_kendaraan;
            $tiket->tanggal_keberangkatan = $model->tanggal_keberangkatan;
            $tiket->status_keberangkatan = 1;
            $tiket->total_harga = $kendaraan->harga_tiket + (($model->jumlah_penumpang)-1)*10000 ;
            $tiket->waktu_transaksi = date('Y:m:d H:i:s',strtotime('+5Hours'));
            $tiket->save(false);
            //Yii::$app->session->setFlash('success', 'Tiket  ' .$model->NIK. ' berhasil dibuat');
            return $this->actionPrintTiket();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing tiket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id_tiket
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $barcode)
    {
        $model = $this->findModel($id, $barcode);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_tiket' => $model->id, 'barcode' => $model->barcode]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing tiket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_tiket
     * @param string $barcode
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $barcode)
    {
        $this->findModel($id, $barcode)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the tiket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_tiket
     * @param string $barcode
     * @return tiket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $barcode)
    {
        if (($model = tiket::findOne(['id_tiket' => $id, 'barcode' => $barcode])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPrintTiket()
    {
        $model = new TiketForm();
        if ($model->load(Yii::$app->request->post())) {
            $kendaraan = harga_tiket::find()
            ->where(['id' => $model->jenis_kendaraan])
            ->one();
        $jenis_kendaraan = $kendaraan->jenis_kendaraan;
        $harga = $kendaraan->harga_tiket + (($model->jumlah_penumpang)-1)*10000 ;
        $pdf_content = $this->renderPartial('cetak', [
            'model' => $model,
            'jenis_kendaraan' => $jenis_kendaraan,
            'harga' => $harga,
        ]);
        }
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->showImageErrors = true;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }
}
