<?php

namespace frontend\controllers;

use Yii;
use frontend\models\kapal;
use frontend\models\PerubahanKapal;
use frontend\models\KapalForm;
use frontend\models\search\kapalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KapalController implements the CRUD actions for kapal model.
 */
class KapalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all kapal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new kapalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single kapal model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new kapal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KapalForm();
        $kapal = new kapal();
        if ($model->load(Yii::$app->request->post())) {
            $kapal->setId();
            $kapal->nama = $model->nama;
            $kapal->tahun_keluaran = $model->tahun_keluaran;
            $kapal->jumlah_muatan = $model->jumlah_muatan;
            $kapal->save();
                Yii::$app->session->setFlash('success', 'Kapal berhasil dibuat');
                return $this->redirect(['kapal/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing kapal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_perubahan = new PerubahanKapal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $model_perubahan->setId();
        $model_perubahan->id_user = Yii::$app->user->getId();
        $model_perubahan->id_kapal = $model->id;
        $model_perubahan->waktu_perubahan_kapal = date('Y:m:d H:i:s',strtotime('+5Hours'));
        $model_perubahan->save();

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing kapal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the kapal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return kapal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = kapal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelJadwal($id)
    {
        if (($model = jadwal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
