<?php

namespace frontend\controllers;

use Yii;
use frontend\models\jadwal;
use frontend\models\PerubahanJadwal;
use frontend\models\JadwalForm;
use frontend\models\search\jadwalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JadwalController implements the CRUD actions for jadwal model.
 */
class JadwalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all jadwal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new jadwalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single jadwal model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new jadwal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JadwalForm();
        $jadwal = new jadwal();

        if ($model->load(Yii::$app->request->post())) {
            $jadwal->setId();
            $jadwal->asal = $model->asal;
            $jadwal->tujuan = $model->tujuan;
            $jadwal->trip = $model->trip;
            $jadwal->jam_keberangkatan = $model->jam_keberangkatan;
            $jadwal->save();
            Yii::$app->session->setFlash('success', 'Jadwal berhasil dibuat');
            return $this->redirect(['jadwal/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing jadwal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // var_dump();
        // die();
        $model = $this->findModel($id);
        $model_perubahan = new PerubahanJadwal();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $model_perubahan->setId();
        $model_perubahan->id_user = Yii::$app->user->getId();
        $model_perubahan->id_jadwal = $model->id;
        $model_perubahan->waktu_perubahan_jadwal = date('Y:m:d H:i:s',strtotime('+5Hours'));
        $model_perubahan->save();
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing jadwal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the jadwal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return jadwal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = jadwal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
