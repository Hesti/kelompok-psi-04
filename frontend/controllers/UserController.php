<?php

namespace frontend\controllers;

use Yii;
use frontend\models\user;
use frontend\models\UserForm;
use frontend\models\search\userSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for user model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all user models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new userSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single user model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new user model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();
        $user = new user();

        if ($model->load(Yii::$app->request->post())) {
            $user->setId();
            $user->nama_lengkap = $model->nama_lengkap;
            $user->tanggal_lahir= $model->tanggal_lahir;
            $user->tempat_lahir = $model->tempat_lahir;
            $user->alamat = $model->alamat;
            $user->no_hp = $model->no_hp;
            // var_dump($model->no_hp);
            // die();
            $user->username = $model->username;
            $user->password = Yii::$app->security->generatePasswordHash($model->password);
            $user->role = "3";
            $user->pertanyaan_pemulihan =  $model->pertanyaan_pemulihan;
            $user->jawaban_pemulihan = $model->jawaban_pemulihan;
            // var_dump($model->jawaban_pemulihan,$model->pertanyaan_pemulihan);
            // die();
            $user->waktu_pembuatan =  date('Y:m:d H:i:s',strtotime('+5Hours'));
            $user->last_login=  date('Y:m:d H:i:s',strtotime('+5Hours'));
            $user->waktu_perubahan_password =  date('Y:m:d H:i:s',strtotime('+5Hours'));

            $user->save();

                    Yii::$app->session->setFlash('success', 'Akun  ' .$model->username. ' berhasil dibuat');
                    return $this->redirect(['user/index']);
            } 
            

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing user model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_user]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing user model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the user model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return user the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = user::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
