<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\manifest */

$this->title = 'Create Manifest';
$this->params['breadcrumbs'][] = ['label' => 'Manifests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manifest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'penumpang' => $penumpang
    ]) ?>

</div>
