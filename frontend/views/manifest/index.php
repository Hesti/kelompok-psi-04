<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\manifestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manifests';

?>
<div class="manifest-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NIK',
            'barcode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
