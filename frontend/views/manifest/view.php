<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\manifest */


$this->params['breadcrumbs'][] = ['label' => 'Manifests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="manifest-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'NIK',
            'barcode',
        ],
    ]) ?>
    <p>
        <?= Html::a('Buat Manifest', ['create', 'NIK' => $model->NIK, 'barcode'=> $model->barcode], ['class' => 'btn btn-success']) ?>
    </p>
</div>
