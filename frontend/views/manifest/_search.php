<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\manifestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manifest-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_manifest') ?>

    <?= $form->field($model, 'barcode') ?>

    <?= $form->field($model, 'nama_lengkap_penumpang') ?>

    <?= $form->field($model, 'usia') ?>

    <?= $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'jenis_kelamin') ?>

    <?php // echo $form->field($model, 'alamat_lengkap_penumpang') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
