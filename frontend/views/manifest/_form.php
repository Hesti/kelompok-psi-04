<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\manifest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manifest-form">
<?php 
    $banyak = $penumpang->jumlah_penumpang;
?>
<?php $form = ActiveForm::begin(); ?>
<table>
    <?php for($i = 0; $i < $banyak-1; ++$i): ?>
        <tr>
            <td><?= $form->field($model, 'nama_lengkap_penumpang')->textInput(['maxlength' => true]) ?></td>
            <td><?= $form->field($model, 'usia')->textInput() ?></td>
            <td><?= $form->field($model, 'kategori')->textInput() ?></td>
            <td><?= $form->field($model, 'jenis_kelamin')->textInput() ?></td>
            <td><?= $form->field($model, 'alamat_lengkap_penumpang')->textInput(['maxlength' => true]) ?></td>
            </td>
        </tr>
    <?php endfor; ?>
</table>
<div style="text-align:right; width:100%; padding:0;">           
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    <?= Html::a('Batal', ['/kapal/index'], ['class'=>'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>
