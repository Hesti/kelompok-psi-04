<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\manifest */

$this->title = 'Update Manifest: ' . $model->id_manifest;
$this->params['breadcrumbs'][] = ['label' => 'Manifests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_manifest, 'url' => ['view', 'id_manifest' => $model->id_manifest, 'barcode' => $model->barcode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="manifest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
