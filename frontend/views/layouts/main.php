<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

 <div class="wrap">
<div class="logo">
  <?php echo Html::img('@web/logo/logo.jpg');?>
 
 </div>
    <?php
    
    
    NavBar::begin([
        
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);

    
    if (Yii::$app->user->isGuest) {
        $menuItems = [
            ['label' => 'Tiket', 'url' => ['/tiket/index']],
          ];
    } else {
        if(Yii::$app->user->identity->role == 3){
            $menuItems = [ 
              ['label' => 'Tiket', 'url' => ['/tiket/index']],
              ['label' => 'Manifest', 'url' => ['/manifest/index']]];
        }
        elseif(Yii::$app->user->identity->role == 2) {
            $menuItems = [
                ['label' => 'Pegawai Loket', 'url' => ['/user/index']],
                 ['label' => 'Kapal', 'url' => ['/kapal/index']],
              ['label' => 'Jadwal', 'url' => ['/jadwal/index']],
              ['label' => 'Harga Tiket', 'url' => ['/harga_tiket/index']],
              ];
        }
        else{
            $menuItems = [
                ['label' => 'Pegawai Loket', 'url' => ['/user/index']],
                 ['label' => 'Kapal', 'url' => ['/kapal/index']],
              ['label' => 'Jadwal', 'url' => ['/jadwal/index']],
              ['label' => 'Harga Tiket', 'url' => ['/harga_tiket/index']],
              ['label' => 'Penumpang', 'url' => ['/penumpang/index']],
              ['label' => 'Grafik', 'url' => ['/site/grafik']]];
        }
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
*/