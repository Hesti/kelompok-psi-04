<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';

?>
<style>

    .radius{
        border-radius: 50px;
        padding: 10px;
    }
</style>
    
<div class="row">    

        <div class="col-lg-3"></div>
        <div class="col-lg-5">
        <h1><?= Html::encode($this->title) ?></h1>
        
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                
                <div class="col-left">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                
</div>

                

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>