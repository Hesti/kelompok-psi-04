<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;


$this->title = 'Kapal';
?>
<div class="site-kapal">
    
    <div class="row">
        <h3> Buat Data Kapal </h3>
      
        <div class="col-lg-7">
            
            <?php $form = ActiveForm::begin(['id' => 'form-kapal']); ?>

                <?= $form->field($model, 'nama_kapal')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tahun_kapal')->textInput(['autofocus' => true]) ?>

                 <?= $form->field($model, 'jumlah_muatan')->textInput(['autofocus' => true]) ?>
                 <?= $form->field($model, 'pendapatan_kapal')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tujuan[]')->dropDownList(
                            ['a' => '', 'b' => 'Truk', 'c' => 'Mobil']
                    ); ?>
                <?= $form->field($model, 'jadwal_kapal[]')->dropDownList(
                            ['a' => '', 'b' => 'Truk', 'c' => 'Mobil']
                    ); ?>
               
               
                
                   <?= Html::button('Batal', ['class' => 'btn btn-primary', 'name' => 'kapalbatal-button']) ?>
                  
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'kapalsimpan-button']) ?>
                </div>
               
            <?php ActiveForm::end(); ?>
        </div>
        
          
        
    </div>
               
</div>
