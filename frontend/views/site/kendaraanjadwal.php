<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;


$this->title = 'Kendaraan & Jadwal';
?>
<div class="site-kendaraanjadwal">
    
    <div class="row">
       
               <div class="col-lg-7">
       
            <?php $form = ActiveForm::begin(['id' => 'form-pegawailoket']); ?>
            <div id="pilihan">
            <?= $form->field($model, 'pilihan[]')->dropDownList(
                            ['a' => 'kendaraan', 'b' => 'jadwal']
                    ); ?>
            </div>
         
                <div id="jadwal">
                <?= $form->field($model, 'jenis')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'golongan')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Batal', ['class' => 'btn btn-primary', 'name' => 'pegawailoketbatal-button']) ?>
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'pegawailoketsimpan-button']) ?>
                </div>
               
            <?php ActiveForm::end(); ?>
        </div>
        
          
        
    </div>
               
</div>
