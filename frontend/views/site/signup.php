<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

$this->title = 'Tiket';
?>
<div class="site-signup">
    
    <div class="row">
        
        <div class="col-lg-5">
            
            <?php $form = ActiveForm::begin(['id' => 'form-tiket']); ?>

                <?= $form->field($model, 'NIK')->textInput(['autofocus' => true]) ?>
                
                <?= $form->field($model, 'jenis_kendaraan[]')->dropDownList(
                            ['a' => '', 'b' => 'Truk', 'c' => 'Mobil']
                    ); ?>
                <?= $form->field($model, 'jumlah_penumpang')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tujuan')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'no_polisi')->textInput(['autofocus' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
               
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                               
                <?= $form->field($model, 'tanggal_keberangkatan')->widget(
                    DatePicker::className(), [
                        // inline too, not bad
                        'inline' => false, 
                        // modify template for custom rendering
                        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                ]);?>

                <?php
                echo $form->field($model, 'jam_keberangkatan[]')->dropDownList(
                            ['a' => '', 'b' => '08:00', 'c' => '13:00']
                    ); ?>

                
               
             
            <?php ActiveForm::end(); ?>
        </div>
    </div>
                </form>
</div>
