<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\jadwal;


$this->title = 'Kapal';
?>
<div class="site-kapal">
    
    <div class="row">

      
        <div class="col-lg-7">
            
            <?php $form = ActiveForm::begin(['id' => 'form-kapal']); ?>

                <?= $form->field($model, 'nama')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tahun_keluaran')->textInput(['maxlength' => 4]) ?>

                 <?= $form->field($model, 'jumlah_muatan')->textInput(['maxlength' => 3]) ?>

                   
               <div style="text-align:right; width:100%; padding:0;">    
               <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Batal', ['/kapal/index'], ['class'=>'btn btn-primary']) ?>
                </div>
               </div>
            <?php ActiveForm::end(); ?>
        </div>
        
          
        
    </div>
               
</div>
