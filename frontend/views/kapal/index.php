<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\kapalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kapal';

?>
<div class="kapal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <div style="text-align:right; width:100%; padding:0;">
        <?= Html::a('Buat Kapal', ['create'], ['class' => 'btn btn-success']) ?>
    </div></p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nama',
            'tahun_keluaran',
            'jumlah_muatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
