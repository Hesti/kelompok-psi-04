<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\kapal */

$this->title = 'Buat Kapal';

?>
<div class="kapal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
