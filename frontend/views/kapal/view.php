<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\kapal */



\yii\web\YiiAsset::register($this);
?>
<div class="kapal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama',
            'tahun_keluaran',
            'jumlah_muatan',
        ],
    ]) ?>
<div style="text-align:right; width:100%; padding:0;">
<p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah anda yakin menghapus data ini?',
                'method' => 'post',
            ],
        ]) ?>
    </p></div>

</div>
