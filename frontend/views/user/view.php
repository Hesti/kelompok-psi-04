<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\user */

$this->title = $model->nama_user;

\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama_lengkap',
            'tanggal_lahir',
            'tempat_lahir',
            'alamat',
            'no_hp',
            'username',
            'pertanyaan_pemulihan',
            'jawaban_pemulihan',
            'waktu_pembuatan',
            'last_login',
            'waktu_perubahan_password',
        ],
    ]) ?>
<div style="text-align:right; width:100%; padding:0;">
<p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

</div>
