<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

$this->title = 'Pegawai Loket';
?>
<div class="site-user">

    <div class="row">
      
        <div class="col-lg-9">
            
            <?php $form = ActiveForm::begin(['id' => 'form-user']); ?>

                <?= $form->field($model, 'nama_lengkap')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tempat_lahir')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tanggal_lahir')->widget(
                    DatePicker::className(), [
                        // inline too, not bad
                        'inline' => false, 
                        // modify template for custom rendering
                        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                ]);?>
                 <?= $form->field($model, 'alamat')->textInput(['autofocus' => true]) ?>
                 <?= $form->field($model, 'no_hp')->textInput(['maxlength' => 12]) ?>
                 <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pertanyaan_pemulihan')->dropDownList(
                            ['Apa warna kesukaan anda?' => 'Apa warna kesukaan anda?', 'Apa hewan kesukaan anda?' => 'Apa hewan kesukaan anda?', 'Apa lagu kesukaan anda sepanjang masa?' => 'Apa lagu kesukaan anda sepanjang masa?']
                    ); ?>
                <?= $form->field($model, 'jawaban_pemulihan')->textInput(['maxlength' => true]) ?>
                <div style="text-align:right; width:100%; padding:0;">
                <div class="form-group">
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Batal', ['/user/index'], ['class'=>'btn btn-primary']) ?>
                </div>
                </div>
               
            <?php ActiveForm::end(); ?>
        </div>
        
          
        
    </div>
               
</div>
