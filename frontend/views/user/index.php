<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pegawai Loket';

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div style="text-align:right; width:100%; padding:0;">
    <p>
        <?= Html::a('Buat Akun', ['create'], ['class' => 'btn btn-success']) ?>
    </p> </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nama_lengkap',
            'tanggal_lahir',
            'tempat_lahir',
            'alamat',
            'no_hp',
            'username',
            //'password',
            //'role',
            //'pertanyaan_pemulihan',
            //'jawaban_pemulihan',
            //'waktu_pembuatan',
            //'last_login',
            //'waktu_perubahan_password',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
