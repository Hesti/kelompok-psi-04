<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\harga_tiket */

$this->title = $model->jenis_kendaraan;

\yii\web\YiiAsset::register($this);
?>
<div class="harga-tiket-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'golongan_kendaraan',
            'jenis_kendaraan',
            'harga_tiket',
        ],
    ]) ?>

<div style="text-align:right; width:100%; padding:0;">
    <p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

</div>
