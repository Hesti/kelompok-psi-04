<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\harga_tiket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="harga-tiket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'golongan_kendaraan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_kendaraan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga_tiket')->textInput() ?>
    <div style="text-align:right; width:100%; padding:0;">
    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Batal', ['/harga_tiket/index'], ['class'=>'btn btn-primary']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
