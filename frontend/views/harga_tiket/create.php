<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\harga_tiket */

$this->title = 'Buat Harga Tiket';

?>
<div class="harga-tiket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
