<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\harga_tiketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Harga Tiket';

?>
<div class="harga-tiket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div style="text-align:right; width:100%; padding:0;">
    <p>
        <?= Html::a('Buat Harga Tiket', ['create'], ['class' => 'btn btn-success']) ?>
    </p></div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'golongan_kendaraan',
            'jenis_kendaraan',
            'harga_tiket',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
