<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\penumpang */

$this->title = $model->barcode;
$this->params['breadcrumbs'][] = ['label' => 'Penumpangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="penumpang-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'barcode' => $model->barcode, 'NIK' => $model->NIK], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'barcode' => $model->barcode, 'NIK' => $model->NIK], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'barcode',
            'NIK',
            'id_user',
            'id_tiket',
            'jumlah_penumpang',
            'no_polisi',
        ],
    ]) ?>

</div>
