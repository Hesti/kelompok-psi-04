<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\penumpang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penumpang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NIK')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'jumlah_penumpang')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'no_polisi')->textInput(['autofocus' => true]) ?>
                <div style="text-align:right; width:100%; padding:0;">
                <div class="form-group">
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Batal', ['/tiket/index'], ['class'=>'btn btn-primary']) ?>
                </div> </div>
    <?php ActiveForm::end(); ?>

</div>
