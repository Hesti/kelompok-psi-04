<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\penumpang */

$this->title = 'Update Penumpang: ' . $model->barcode;
$this->params['breadcrumbs'][] = ['label' => 'Penumpangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->barcode, 'url' => ['view', 'barcode' => $model->barcode, 'NIK' => $model->NIK]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penumpang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
