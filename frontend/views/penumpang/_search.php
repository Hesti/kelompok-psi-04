<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\penumpangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penumpang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'barcode') ?>

    <?= $form->field($model, 'NIK') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'id_tiket') ?>

    <?= $form->field($model, 'jumlah_penumpang') ?>

    <?php // echo $form->field($model, 'no_polisi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
