<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\chartjs\ChartJs;
use frontend\models\Penumpang;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\penumpangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penumpang';
$this->params['breadcrumbs'][] = $this->title;

$a = array();
$rows = (new \yii\db\Query())
    ->select(['jumlah_penumpang'])
    ->from('penumpang')
    ->all();


for($i = 0 ; $i < count($rows) ; $i++){
    array_push($a, $rows[$i]['jumlah_penumpang']);
}

$res = (new \yii\db\Query())
    ->select(['sum(jumlah_penumpang) as jumlah', 'tanggal_keberangkatan'])
    ->from('tiket')
    ->join('INNER JOIN', 'penumpang', 'tiket.barcode = penumpang.barcode')
    ->groupBy('tanggal_keberangkatan')
    ->all();

$jumlah = array();
$tanggal = array();
for($i = 0 ; $i < count($res) ; $i++){
    array_push($jumlah, $res[$i]['jumlah']);
    array_push($tanggal, $res[$i]['tanggal_keberangkatan']);
}

?>
<div class="penumpang-index">

    <?= ChartJs::widget([
    'type' => 'line',
    'options' => [
        'height' => 150,
        'width' => 300
    ],
    'data' => [
        'labels' => $tanggal,
        'datasets' => [
            [
                'label' => "Jumlah Penumpang",
                'backgroundColor' => "rgba(179,181,198,0.2)",
                'borderColor' => "rgba(179,181,198,1)",
                'pointBackgroundColor' => "rgba(179,181,198,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(179,181,198,1)",
                'data' => $jumlah,
            ]
        ]
    ]
]);
?>


</div>
