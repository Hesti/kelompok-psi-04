<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\jadwal */



\yii\web\YiiAsset::register($this);
?>
<div class="jadwal-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'asal',
            'tujuan',
            'trip',
            'jam_keberangkatan',
        ],
    ]) ?>
<div style="text-align:right; width:100%; padding:0;">   
     <p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id_jadwal], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id_jadwal], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah anda yakin menghapus data ini?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

</div>
