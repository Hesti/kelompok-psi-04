<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\jadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'asal')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'tujuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jam_keberangkatan')->textInput() ?>
    <div style="text-align:right; width:100%; padding:0;">  
    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Batal', ['/jadwal/index'], ['class'=>'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
