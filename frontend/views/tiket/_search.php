<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\tiketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiket-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'barcode') ?>

    <?= $form->field($model, 'id_pegawai_loket') ?>

    <?= $form->field($model, 'id_kapal') ?>

    <?= $form->field($model, 'id_jadwal') ?>

    <?php // echo $form->field($model, 'id_harga_tiket') ?>

    <?php // echo $form->field($model, 'tanggal_keberangkatan') ?>

    <?php // echo $form->field($model, 'status_keberangkatan') ?>

    <?php // echo $form->field($model, 'total_harga') ?>

    <?php // echo $form->field($model, 'waktu_transaksi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
