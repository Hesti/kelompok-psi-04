<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\modules\askm\assets\AskmAsset;

/* @var $this yii\web\View */
/* @var $model backend\modules\askm\models\IzinBermalam */

$this->title = 'Surat Izin Bermalam Mahasiswa';



$date_berangkat = date_create($model->asal);
$date_kembali = date_create($model->tujuan);

?>
    <div class="izin-bermalam-view" style="font-family: sans-serif; font-size: 11">

    <h2 style="text-align: center;"><strong>TIKET KAPAL FERRY</strong></h2>
<h2 style="text-align: center;"><strong>PELABUHAN AJIBATA</strong></h2>
<p>&nbsp;</p>
<p>NIK&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 34141</p>
<p>Jumlah Penumpang&nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $model->jumlah_penumpang?></p>
<p>Jenis Kendaraan&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $jenis_kendaraan?></p>
<p>Plat Kendaraan&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $model->no_polisi?></p>
<p>Rute&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: <?php echo $model->asal. '-' .$model->tujuan?></p>
<p>Trip&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : <?php echo $model->trip?></p>
<p>Tanggal Keberangkatan : <?php echo $model->tanggal_keberangkatan?></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Total Harga :</p>
<h2><strong><?php echo $harga?></strong></h2>
</div>