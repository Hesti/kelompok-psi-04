<?php

use yii\helpers\Html;

// @var $this yii\web\View;
// @var $model frontend\models\tiket;
use frontend\models\tiket;




?>
<div class="tiket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
