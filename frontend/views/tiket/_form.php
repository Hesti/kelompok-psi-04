<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use frontend\models\jadwal;
use frontend\models\harga_tiket;
use frontend\models\tiket;
use frontend\models\kapal;


$this->title = 'Tiket';
?>
<div class="tiket">
    <div class="row">
        <div class="col-lg-9">
            <?php $form = ActiveForm::begin(['id' => 'form-tiket']); ?>
                <?= $form->field($model, 'NIK')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'jenis_kendaraan',[
                                        'horizontalCssClasses' => [
                                            'wrapper' => 'col-sm-6',
                                            'label' => 'col-sm-1',
                                        ]
                                    ])->dropDownList(
                                        ArrayHelper::map(harga_tiket::find()->all(),'id', 'jenis_kendaraan'),
                                        [
                                            'prompt' => '- Pilih Jenis Kendaraan -',
                                        ])->label('Jenis Kendaraan')?>

                <?= $form->field($model, 'jumlah_penumpang')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'asal',[
                                        'horizontalCssClasses' => [
                                            'wrapper' => 'col-sm-6',
                                            'label' => 'col-sm-1',
                                        ]
                                    ])->dropDownList(
                                        ArrayHelper::map(jadwal::find()->select('asal, asal')->groupBy('asal')->all(),'asal', 'asal'),
                                        [
                                            'prompt' => '- Pilih Asal -',
                                        ])->label('Asal')?>
                <?= $form->field($model, 'tujuan',[
                                        'horizontalCssClasses' => [
                                            'wrapper' => 'col-sm-6',
                                            'label' => 'col-sm-1',
                                        ]
                                    ])->dropDownList(
                                        ArrayHelper::map(jadwal::find()->select('tujuan, tujuan')->groupBy('tujuan')->all(),'tujuan', 'tujuan'),
                                        [
                                            'prompt' => '- Pilih Tujuan -',
                                        ])->label('Tujuan')?>
                                        
                <?= $form->field($model, 'trip',[
                                        'horizontalCssClasses' => [
                                            'wrapper' => 'col-sm-6',
                                            'label' => 'col-sm-1',
                                        ]
                                    ])->dropDownList(
                                        ArrayHelper::map(jadwal::find()->select('trip, trip')->groupBy('trip')->all(),'trip', 'trip'),
                                        [
                                            'prompt' => '- Pilih Trip -',
                                        ])->label('Trip')?>
            
                <?= $form->field($model, 'no_polisi')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'tanggal_keberangkatan')->widget(
                        DatePicker::className(), [
                            // inline too, not bad
                            'inline' => false, 
                            // modify template for custom rendering
                            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                    ]);?>

                <?= $form->field($model, 'kapal',[
                                        'horizontalCssClasses' => [
                                            'wrapper' => 'col-sm-6',
                                            'label' => 'col-sm-1',
                                        ]
                                    ])->dropDownList(
                                        ArrayHelper::map(kapal::find()->select('id, nama')->all(),'id', 'nama'),
                                        [
                                            'prompt' => '- Pilih Kapal -',
                                        ])->label('Kapal')?>
                <div style="text-align:right; width:100%; padding:0;">
                <div class="form-group">
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Batal', ['/tiket/index'], ['class'=>'btn btn-primary']) ?>
                </div>
                </div>
                            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
