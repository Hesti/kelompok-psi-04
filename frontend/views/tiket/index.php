<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\tiketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tiket';

?>
<div class="tiket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div style="text-align:right; width:100%; padding:0;">
    <p>
        <?= Html::a('Buat Tiket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'barcode',
                'value' => 'penumpang.NIK', 
            ],
            //'id',
            //'barcode',
            //'id_user',
            //'id_kapal',
            //'id_jadwal',
            //'id_harga_tiket',
            'tanggal_keberangkatan',
            'status_keberangkatan',
            'total_harga',
            'waktu_transaksi',

            ['class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>


</div>
