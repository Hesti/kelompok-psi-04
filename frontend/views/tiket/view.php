<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\tiket */


$this->params['breadcrumbs'][] = ['label' => 'Tikets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tiket-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'barcode',
            'id_user',
            'id_kapal',
            'id_jadwal',
            'id_harga_tiket',
            'tanggal_keberangkatan',
            'status_keberangkatan',
            'total_harga',
            'waktu_transaksi',
        ],
    ]) ?>

<div style="text-align:right; width:100%; padding:0;">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'barcode' => $model->barcode], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'barcode' => $model->barcode], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

</div>
