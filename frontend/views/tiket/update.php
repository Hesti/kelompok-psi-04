<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\tiket */

$this->title = 'Update Tiket: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tikets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id_tiket' => $model->id, 'barcode' => $model->barcode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tiket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
