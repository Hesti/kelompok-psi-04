<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Kapal form
 */
class KapalForm extends Model
{
    public $nama;
    public $tahun_keluaran;
    public $jumlah_muatan;
    public $asal;
    public $tujuan;
    public $trip;



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['nama', 'trim'],
            ['nama', 'required'],
            ['nama', 'string', 'min' => 2, 'max' => 255],

            ['tahun_keluaran', 'trim'],
            ['tahun_keluaran', 'required'],
            ['tahun_keluaran', 'string', 'max' => 255],
        
            ['jumlah_muatan', 'trim'],
            ['jumlah_muatan', 'required'],
            ['jumlah_muatan', 'string', 'max' => 255],

            ['asal', 'trim'],
            ['asal', 'required'],
            ['asal', 'string', 'max' => 255],

            ['tujuan', 'trim'],
            ['tujuan', 'required'],
            ['tujuan', 'string', 'max' => 255],

            ['trip', 'trim'],
            ['trip', 'required'],
            ['trip', 'string', 'max' => 255],

        ];
    }



}
