<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $nama_lengkap
 * @property string $tanggal_lahir
 * @property string $tempat_lahir
 * @property string $alamat
 * @property int $no_hp
 * @property string $username
 * @property string $password
 * @property string $role
 * @property string $pertanyaan_pemulihan
 * @property string $jawaban_pemulihan
 * @property string $waktu_pembuatan
 * @property string $last_login
 * @property string $waktu_perubahan_password
 */
class UserForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $id;
    public $nama_lengkap;
    public $tanggal_lahir;
    public $tempat_lahir;
    public $alamat;
    public $no_hp;
    public $username;
    public $password;
    public $pertanyaan_pemulihan;
    public $jawaban_pemulihan;

    public function rules()
    {
        return [
            [['id', 'nama_lengkap', 'tanggal_lahir', 'tempat_lahir', 'alamat', 'no_hp', 'username', 'password', 'role', 'pertanyaan_pemulihan', 'jawaban_pemulihan'], 'required'],
            [['tanggal_lahir', 'waktu_pembuatan', 'last_login', 'waktu_perubahan_password'], 'safe'],
            [['id', 'nama_lengkap', 'jawaban_pemulihan'], 'string', 'max' => 64],
            [['tempat_lahir'], 'string', 'max' => 32],
            [['alamat', 'password'], 'string', 'max' => 128],
            [['no_hp'], 'string', 'max' => 12],
            [['username'], 'string', 'max' => 8],
            [['role'], 'string', 'max' => 16],
            [['id'], 'unique'],
            [['pertanyaan_pemulihan'],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'nama_lengkap' => 'Nama Lengkap',
            'tanggal_lahir' => 'Tanggal Lahir',
            'tempat_lahir' => 'Tempat Lahir',
            'alamat' => 'Alamat',
            'no_hp' => 'No Hp',
            'username' => 'Username',
            'password' => 'Password',
            'role' => 'Role',
            'pertanyaan_pemulihan' => 'Pertanyaan Pemulihan',
            'jawaban_pemulihan' => 'Jawaban Pemulihan',
            'waktu_pembuatan' => 'Waktu Pembuatan',
            'last_login' => 'Last Login',
            'waktu_perubahan_password' => 'Waktu Perubahan Password',
        ];
    }
}
