<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tiket".
 *
 * @property string $id
 * @property string $barcode
 * @property string $id_user
 * @property string $id_kapal
 * @property string $id_jadwal
 * @property string $id_harga_tiket
 * @property string $tanggal_keberangkatan
 * @property int $status_keberangkatan
 * @property int $total_harga
 * @property string $waktu_transaksi
 *
 * @property Penumpang[] $penumpangs
 * @property Harga_Tiket $harga_Tiket
 * @property Jadwal $jadwal
 * @property Kapal $kapal
 * @property User $user
 * @property Penumpang $barcode0
 */
class tiket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barcode', 'id_user', 'id_kapal', 'id_jadwal', 'id_harga_tiket', 'tanggal_keberangkatan', 'status_keberangkatan', 'total_harga'], 'required'],
            [['tanggal_keberangkatan', 'waktu_transaksi'], 'safe'],
            [['status_keberangkatan', 'total_harga'], 'integer'],
            [['id', 'id_user', 'id_jadwal', 'id_kapal', 'id_harga_tiket'], 'string', 'max' => 64],
            [['barcode'], 'string', 'max' => 16],
            [['id', 'barcode'], 'unique', 'targetAttribute' => ['id', 'barcode']],
            [['id_harga_tiket'], 'exist', 'skipOnError' => true, 'targetClass' => Harga_Tiket::className(), 'targetAttribute' => ['id_harga_tiket' => 'id_harga_tiket']],
            [['id_jadwal'], 'exist', 'skipOnError' => true, 'targetClass' => Jadwal::className(), 'targetAttribute' => ['id_jadwal' => 'id_jadwal']],
            [['id_kapal'], 'exist', 'skipOnError' => true, 'targetClass' => Kapal::className(), 'targetAttribute' => ['id_kapal' => 'id_kapal']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
            [['barcode'], 'exist', 'skipOnError' => true, 'targetClass' => Penumpang::className(), 'targetAttribute' => ['barcode' => 'barcode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'barcode' => 'Barcode',
            'id_user' => 'Id User',
            'id_jadwal' => 'Id Jadwal',
            'id_harga_tiket' => 'Id Harga Tiket',
            'tanggal_keberangkatan' => 'Tanggal Keberangkatan',
            'status_keberangkatan' => 'Status Keberangkatan',
            'total_harga' => 'Total Harga',
            'waktu_transaksi' => 'Waktu Transaksi',
        ];
    }

    public function setId()
    {
        $this->id = Yii::$app->security->generateRandomString(64);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenumpangs()
    {
        return $this->hasMany(Penumpang::className(), ['id_tiket' => 'id_tiket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHargaTiket()
    {
        return $this->hasOne(Harga_Tiket::className(), ['id_harga_tiket' => 'id_harga_tiket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwal()
    {
        return $this->hasOne(Jadwal::className(), ['id_jadwal' => 'id_jadwal']);
    }

    public function getKapal()
    {
        return $this->hasOne(Kapal::className(), ['id_kapal' => 'id_kapal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenumpang()
    {
        return $this->hasOne(Penumpang::className(), ['barcode' => 'barcode']);
    }
    public function setbarcode()
    {
        $this->barcode = Yii::$app->security->generateRandomString(64);
    }

}
