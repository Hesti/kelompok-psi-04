<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $pertanyaan_pemulihan;
    public $jawaban_pemulihan;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['pertanyaan_pemulihan', 'trim'],
            ['pertanyaan_pemulihan', 'required'],
            ['pertanyaan_pemulihan', 'exist',
                'targetClass' => '\common\models\User',
            ],
            ['jawaban_pemulihan', 'trim'],
            ['jawaban_pemulihan', 'required'],
            ['jawaban_pemulihan', 'exist',
                'targetClass' => '\common\models\User',
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}
