<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "perubahan_jadwal".
 *
 * @property string $id
 * @property string $id_user
 * @property string $id_jadwal
 * @property string $waktu_perubahan_jadwal
 *
 * @property Jadwal $jadwal
 * @property User $user
 */
class PerubahanJadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perubahan_jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_jadwal'], 'required'],
            [['waktu_perubahan_jadwal'], 'safe'],
            [['id', 'id_user', 'id_jadwal'], 'string', 'max' => 64],
            [['id'], 'unique'],
            [['id_jadwal'], 'exist', 'skipOnError' => true, 'targetClass' => Jadwal::className(), 'targetAttribute' => ['id_jadwal' => 'id_jadwal']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_user' => 'Id User',
            'id_jadwal' => 'Id Jadwal',
            'waktu_perubahan_jadwal' => 'Waktu Perubahan Jadwal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwal()
    {
        return $this->hasOne(Jadwal::className(), ['id_jadwal' => 'id_jadwal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    public function setId()
    {
        $this->id= Yii::$app->security->generateRandomString(64);
    }
}
