<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "manifest".
 *
 * @property string $id_manifest
 * @property string $barcode
 * @property string $nama_lengkap_penumpang
 * @property int $usia
 * @property int $kategori
 * @property int $jenis_kelamin
 * @property string $alamat_lengkap_penumpang
 *
 * @property Penumpang $barcode0
 */
class manifest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manifest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barcode', 'nama_lengkap_penumpang', 'usia', 'kategori', 'jenis_kelamin', 'alamat_lengkap_penumpang'], 'required'],
            [['usia'], 'integer'],
            [['id', 'nama_lengkap_penumpang','kategori','jenis_kelamin'], 'string', 'max' => 64],
            [['barcode'], 'string', 'max' => 16],
            [['alamat_lengkap_penumpang'], 'string', 'max' => 128],
            [['id', 'barcode'], 'unique', 'targetAttribute' => ['id', 'barcode']],
            [['barcode'], 'exist', 'skipOnError' => true, 'targetClass' => Penumpang::className(), 'targetAttribute' => ['barcode' => 'barcode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarcode0()
    {
        return $this->hasOne(Penumpang::className(), ['barcode' => 'barcode']);
    }
}
