<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "perubahan_kapal".
 *
 * @property string $id
 * @property string $id_user
 * @property string $id_kapal
 * @property string $waktu_perubahan_kapal
 *
 * @property Kapal $kapal
 * @property User $user
 */
class PerubahanKapal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perubahan_kapal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_kapal'], 'required'],
            [['waktu_perubahan_kapal'], 'safe'],
            [['id', 'id_user', 'id_kapal'], 'string', 'max' => 64],
            [['id'], 'unique'],
            [['id_kapal'], 'exist', 'skipOnError' => true, 'targetClass' => Kapal::className(), 'targetAttribute' => ['id_kapal' => 'id_kapal']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_user' => 'Id User',
            'id_kapal' => 'Id Kapal',
            'waktu_perubahan_kapal' => 'Waktu Perubahan Kapal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKapal()
    {
        return $this->hasOne(Kapal::className(), ['id_kapal' => 'id_kapal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    public function setId()
    {
        $this->id = Yii::$app->security->generateRandomString(64);
    }
}
