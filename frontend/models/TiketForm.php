<?php
namespace frontend\models;

use Yii;
use yii\base\Model;


/**
 * Signup form
 */
class TiketForm extends Model
{
    public $NIK;
    public $jenis_kendaraan;
    public $jumlah_penumpang;
    public $tujuan;
    public $no_polisi;
    public $tanggal_keberangkatan;
    public $trip;
    public $asal;
    public $kapal;



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['NIK', 'trim'],
            ['NIK', 'required'],
            ['NIK', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['NIK', 'integer'],

            ['jenis_kendaraan', 'trim'],
            ['jenis_kendaraan', 'required'],
            ['jenis_kendaraan', 'string', 'max' => 255],
            ['rute', 'trim'],
            ['rute', 'required'],
            ['rute', 'string', 'max' => 255],
        
            ['jumlah_penumpang', 'trim'],
            ['jumlah_penumpang', 'required'],
            ['jumlah_penumpang', 'string', 'max' => 255],

            ['asal', 'trim'],
            ['asal', 'required'],
            ['asal', 'string', 'max' => 255],

            ['tujuan', 'trim'],
            ['tujuan', 'required'],
            ['tujuan', 'string', 'max' => 255],

            ['kapal', 'trim'],
            ['kapal', 'required'],
            ['kapal', 'string', 'max' => 255],

            ['no_polisi', 'trim'],
            ['no_polisi', 'required'],
            ['no_polisi', 'string', 'max' => 255],

            ['tanggal_keberangkatan', 'trim'],
            ['tanggal_keberangkatan', 'required'],
            ['tanggal_keberangkatan', 'safe'],

            ['trip', 'trim'],
            ['trip', 'required'],
            ['trip', 'string', 'max' => 255],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    
}
