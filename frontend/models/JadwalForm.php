<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property string $id
 * @property string $tujuan
 * @property string $trip
 * @property string $jam_keberangkatan
 *
 * @property JadwalKapal[] $jadwalKapals
 * @property PerubahanJadwal[] $perubahanJadwals
 * @property Tiket[] $tikets
 */
class JadwalForm extends \yii\db\ActiveRecord
{
    public $asal;
    public $tujuan;
    public $trip;
    public $jam_keberangkatan;

    public function rules()
    {
        return [
            [['id', 'asal', 'tujuan', 'trip', 'jam_keberangkatan'], 'required'],
            [['jam_keberangkatan'], 'safe'],
            [['id'], 'string', 'max' => 64],
            [['tujuan', 'asal'], 'string', 'max' => 32],
            [['trip'], 'string', 'max' => 4],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'rute' => 'Rute',
            'trip' => 'Trip',
            'jam_keberangkatan' => 'Jam Keberangkatan',
        ];
    }
    
}
