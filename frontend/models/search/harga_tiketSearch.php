<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\harga_tiket;

/**
 * harga_tiketSearch represents the model behind the search form of `frontend\models\harga_tiket`.
 */
class harga_tiketSearch extends harga_tiket
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'golongan_kendaraan', 'jenis_kendaraan'], 'safe'],
            [['harga_tiket'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = harga_tiket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'harga_tiket' => $this->harga_tiket,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id_harga_tiket])
            ->andFilterWhere(['like', 'golongan_kendaraan', $this->golongan_kendaraan])
            ->andFilterWhere(['like', 'jenis_kendaraan', $this->jenis_kendaraan]);

        return $dataProvider;
    }
}
