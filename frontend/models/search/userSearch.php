<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\user;

/**
 * userSearch represents the model behind the search form of `frontend\models\user`.
 */
class userSearch extends user
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nama_lengkap', 'tanggal_lahir', 'tempat_lahir', 'alamat', 'no_hp', 'username', 'password', 'role', 'pertanyaan_pemulihan', 'jawaban_pemulihan', 'waktu_pembuatan', 'last_login', 'waktu_perubahan_password'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = user::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal_lahir' => $this->tanggal_lahir,
            'no_hp' => $this->no_hp,
            'waktu_pembuatan' => $this->waktu_pembuatan,
            'last_login' => $this->last_login,
            'waktu_perubahan_password' => $this->waktu_perubahan_password,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama_lengkap', $this->nama_lengkap])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'pertanyaan_pemulihan', $this->pertanyaan_pemulihan])
            ->andFilterWhere(['like', 'jawaban_pemulihan', $this->jawaban_pemulihan]);

        return $dataProvider;
    }
}
