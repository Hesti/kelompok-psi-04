<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\tiket;

/**
 * tiketSearch represents the model behind the search form of `frontend\models\tiket`.
 */
class tiketSearch extends tiket
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barcode', 'id_user', 'id_jadwal', 'id_kapal', 'id_harga_tiket', 'tanggal_keberangkatan', 'waktu_transaksi', 'barcode'], 'safe'],
            [['status_keberangkatan', 'total_harga'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = tiket::find();
        $query->joinWith(['penumpang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal_keberangkatan' => $this->tanggal_keberangkatan,
            'status_keberangkatan' => $this->status_keberangkatan,
            'total_harga' => $this->total_harga,
            'waktu_transaksi' => $this->waktu_transaksi,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'id_user', $this->id_user])
            ->andFilterWhere(['like', 'id_jadwal', $this->id_jadwal])
            ->andFilterWhere(['like', 'id_kapal', $this->id_kapal])
            ->andFilterWhere(['like', 'id_harga_tiket', $this->id_harga_tiket])
            ->andFilterWhere(['like', 'penumpang.nik', $this->barcode]);

        return $dataProvider;
    }
}
