<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\manifest;

/**
 * manifestSearch represents the model behind the search form of `frontend\models\manifest`.
 */
class manifestSearch extends manifest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'barcode', 'nama_lengkap_penumpang', 'alamat_lengkap_penumpang'], 'safe'],
            [['usia', 'kategori', 'jenis_kelamin'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = manifest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'usia' => $this->usia,
            'kategori' => $this->kategori,
            'jenis_kelamin' => $this->jenis_kelamin,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'nama_lengkap_penumpang', $this->nama_lengkap_penumpang])
            ->andFilterWhere(['like', 'alamat_lengkap_penumpang', $this->alamat_lengkap_penumpang]);

        return $dataProvider;
    }
}
