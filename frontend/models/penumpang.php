<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "penumpang".
 *
 * @property string $barcode
 * @property int $NIK
 * @property string $id_user
 * @property string $id_tiket
 * @property int $jumlah_penumpang
 * @property string $no_polisi
 *
 * @property Manifest[] $manifests
 * @property User $user
 * @property Tiket $tiket
 * @property Tiket[] $tikets
 */
class penumpang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penumpang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barcode', 'NIK', 'id_user', 'jumlah_penumpang', 'no_polisi'], 'required'],
            [['NIK', 'jumlah_penumpang'], 'integer'],
            [['barcode', 'id_user'], 'string', 'max' => 64],
            [['no_polisi'], 'string', 'max' => 11],
            [['barcode', 'NIK'], 'unique', 'targetAttribute' => ['barcode', 'NIK']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']]
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'barcode' => 'Barcode',
            'NIK' => 'NIK',
            'id_user' => 'Id User',
            'jumlah_penumpang' => 'Jumlah Penumpang',
            'no_polisi' => 'No Polisi',
        ];
    }
    public function setId()
    {
        $this->id = Yii::$app->security->generateRandomString(64);
    }

    public function setBarcode()
    {
        $this->barcode = Yii::$app->security->generateRandomString(64);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManifests()
    {
        return $this->hasMany(Manifest::className(), ['barcode' => 'barcode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiket()
    {
        return $this->hasOne(Tiket::className(), ['id_tiket' => 'id_tiket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['barcode' => 'barcode']);
    }
}
