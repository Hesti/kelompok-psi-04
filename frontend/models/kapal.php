<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kapal".
 *
 * @property string $id
 * @property string $nama_kapal
 * @property int $tahun_keluaran
 * @property int $jumlah_muatan(ton)
 * @property int $pendapatan_kapal(%)
 *
 * @property JadwalKapal[] $jadwalKapals
 * @property PerubahanKapal[] $perubahanKapals
 * @property Tiket[] $tikets
 */
class kapal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kapal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'tahun_keluaran', 'jumlah_muatan'], 'required'],
            [['tahun_keluaran', 'jumlah_muatan'], 'integer'],
            [['id', 'nama'], 'string', 'max' => 64],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'nama' => 'Nama',
            'tahun_keluaran' => 'Tahun Keluaran',
            'jumlah_muatan' => 'Jumlah Muatan(ton)',
        ];
    }

    public function setId()
    {
        $this->id = Yii::$app->security->generateRandomString(64);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwalKapals()
    {
        return $this->hasMany(JadwalKapal::className(), ['id_kapal' => 'id_kapal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerubahanKapals()
    {
        return $this->hasMany(PerubahanKapal::className(), ['id_kapal' => 'id_kapal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['id_kapal' => 'id_kapal']);
    }
}
