<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "perubahan_harga_tiket".
 *
 * @property string $id
 * @property string $id_user
 * @property string $id_harga_tiket
 * @property string $waktu_perubahan_harga
 *
 * @property HargaTiket $hargaTiket
 * @property User $user
 */
class PerubahanHargaTiket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perubahan_harga_tiket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_harga_tiket'], 'required'],
            [['waktu_perubahan_harga'], 'safe'],
            [['id', 'id_user', 'id_harga_tiket'], 'string', 'max' => 64],
            [['id'], 'unique'],
            [['id_harga_tiket'], 'exist', 'skipOnError' => true, 'targetClass' => harga_tiket::className(), 'targetAttribute' => ['id_harga_tiket' => 'id_harga_tiket']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_user' => 'Id User',
            'id_harga_tiket' => 'Id Harga Tiket',
            'waktu_perubahan_harga' => 'Waktu Perubahan Harga',
        ];
    }

    public function setId()
    {
        $this->id= Yii::$app->security->generateRandomString(64);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHargaTiket()
    {
        return $this->hasOne(HargaTiket::className(), ['id_harga_tiket' => 'id_harga_tiket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
