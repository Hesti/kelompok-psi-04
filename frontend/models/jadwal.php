<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property string $id
 * @property string $tujuan
 * @property string $trip
 * @property string $jam_keberangkatan
 *
 * @property JadwalKapal[] $jadwalKapals
 * @property PerubahanJadwal[] $perubahanJadwals
 * @property Tiket[] $tikets
 */
class jadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'asal', 'tujuan', 'trip', 'jam_keberangkatan'], 'required'],
            [['jam_keberangkatan'], 'safe'],
            [['id'], 'string', 'max' => 64],
            [['asal', 'tujuan'], 'string', 'max' => 32],
            [['trip'], 'string', 'max' => 4],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'asal' => 'Asal',
            'tujuan' => 'Tujuan',
            'trip' => 'Trip',
            'jam_keberangkatan' => 'Jam Keberangkatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function setId()
    {
        $this->id = Yii::$app->security->generateRandomString(64);
    }

    public function getJadwalKapals()
    {
        return $this->hasMany(JadwalKapal::className(), ['id_jadwal' => 'id_jadwal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerubahanJadwals()
    {
        return $this->hasMany(PerubahanJadwal::className(), ['id_jadwal' => 'id_jadwal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['id_jadwal' => 'id_jadwal']);
    }
}
