<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "harga_tiket".
 *
 * @property string $id
 * @property string $golongan_kendaraan
 * @property string $jenis_kendaraan
 * @property int $harga_tiket
 *
 * @property PerubahanHargaTiket[] $perubahanHargaTikets
 * @property Tiket[] $tikets
 */
class harga_tiket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'harga_tiket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'golongan_kendaraan', 'jenis_kendaraan', 'harga_tiket'], 'required'],
            [['harga_tiket'], 'number'],
            [['id'], 'string', 'max' => 64],
            [['golongan_kendaraan'], 'string', 'max' => 4],
            [['jenis_kendaraan'], 'string', 'max' => 32],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Harga Tiket',
            'golongan_kendaraan' => 'Golongan Kendaraan',
            'jenis_kendaraan' => 'Jenis Kendaraan',
            'harga_tiket' => 'Harga Tiket',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function setId()
    {
        $this->id_harga_tiket = Yii::$app->security->generateRandomString(64);
    }

    public function getPerubahanHargaTikets()
    {
        return $this->hasMany(PerubahanHargaTiket::className(), ['id_harga_tiket' => 'id_harga_tiket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['id_harga_tiket' => 'id_harga_tiket']);
    }
}
